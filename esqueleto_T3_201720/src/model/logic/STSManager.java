package model.logic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

import com.google.gson.Gson;

import model.data_structures.Cola;
import model.data_structures.DoubleLinkedList;
import model.data_structures.DoubleLinkedList.Node;
import model.data_structures.IStack;

import model.data_structures.NodeSimple;

import model.data_structures.Pila;
import model.data_structures.RingList;
import model.vo.StopVO;
import model.vo.VOTrip;
import model.vo.VOZone;
import model.vo.BusUpdateVO;
import api.ISTSManager;

public class STSManager implements ISTSManager{

	private Cola<BusUpdateVO> actualizacionBus;
	private RingList<StopVO> listaParadas;
	private RingList<VOZone> listaZonas; 

	@Override
	public void readBusUpdate(File rtFile) 
	{
		actualizacionBus = new Cola<BusUpdateVO>();
		Gson gson = new Gson();

		try
		{
			BusUpdateVO[] info = gson.fromJson(new FileReader(rtFile), BusUpdateVO[].class);
			for (int i=0; i<info.length; i++ )
			{
				actualizacionBus.enqueue(info[i]);
			}
		}

		catch (Exception e)
		{
			e.printStackTrace();
		}

	}
	

		// TODO Auto-generated method stub
		
	@Override
	public IStack<StopVO> listStops(Integer tripID) 
	{
		Pila<StopVO> pila = new Pila<StopVO>();
		NodeSimple busxd = actualizacionBus.getFirstNode();
		double lat1=0;
		double long1 = 0;
		
		while (busxd.getNext() != null)
		{
			if(((BusUpdateVO)busxd.getElement()).trip_id().equals(tripID))
			{
				lat1 = Double.parseDouble(((BusUpdateVO)busxd.getElement()).latitude());
				long1 = Double.parseDouble(((BusUpdateVO)busxd.getElement()).longitude());
				
			}
			busxd = busxd.getNext();
		}
	
		for (int i = 0; i < listaParadas.size(); i++)
		{
			double lat2 =Double.parseDouble(((StopVO)listaParadas.getNode(i).getElement()).lat()) ;
			double long2 =Double.parseDouble(((StopVO)listaParadas.getNode(i).getElement()).lon()) ;
			if(getDistance(lat1, long1, lat2, long2) <= 70)
			{
				pila.push((StopVO) listaParadas.getNode(i).getElement());
			}
		}
		
		Pila<StopVO> resp = new Pila<StopVO>();
		
		for (int i = 0; i < pila.getSize(); i++) 
		{
			resp.push(pila.pop());
		}
		
		return resp;
	}

	@Override
	public void loadStops(String stopsFile) 
	{

		try 
		{
			listaParadas = new RingList<StopVO>();
			listaZonas = new RingList<VOZone>();

			BufferedReader br = new BufferedReader(new FileReader(stopsFile));
			String linea = br.readLine();

			while (null!=linea) 
			{
				String [] info = linea.split(",");

				StopVO parada = new StopVO (info[0], info[1], info[2], info[3], info[4], info[5], info[6], info[7], info[8], info[9]);
				listaParadas.addLast(parada);

				linea = br.readLine();
			}

			br.close();

			for (int i = 0; i < listaParadas.size(); i++) 
			{
				boolean sePuedeAgregar = false;

				for (int j = 0; j < listaZonas.size(); j++) 
				{
					if ( !(((StopVO) listaParadas.getElement(i)).zone_id().equals(((VOZone) listaZonas.getElement(j)).zone_id())))
					{
						sePuedeAgregar = true;
					}

					else
					{
						((VOZone) listaZonas.getElement(j)).agregarParada(((StopVO) listaParadas.getElement(i)));
					}
				}

				if (sePuedeAgregar)
				{
					VOZone zona = new VOZone ( ((StopVO) listaParadas.getElement(i)).zone_id() , (StopVO) listaParadas.getElement(i));
					listaZonas.addFirst(zona);
				}

			}

		} 

		catch (Exception e) 
		{
			e.printStackTrace();		
		} 

	}


	public double getDistance(double lat1, double lon1, double lat2, double lon2)
	{
		final double R = 6371*1000; // Radious of the earth
		double latDistance = toRad(lat2-lat1);
		double lonDistance = toRad(lon2-lon1);
		double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2) + Math.cos(toRad(lat1)) * Math.cos(toRad(lat2)) * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
		double distance = R * c;
		
		return distance;
		
	}

	private double toRad( double value)
	{
		return (value * Math.PI / 180);
	}
	
}
